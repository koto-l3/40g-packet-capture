#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/poll.h>

#include <time.h>
#include <locale.h>
//#include <pthread.h>
#include <netinet/udp.h>
#include <netinet/ip.h>
#include <netinet/if_ether.h>
#include <arpa/inet.h> // htons
#include <cstring>
#include <vector>
#include <thread>
#include <memory>
#include <string>
#include <iostream>
#include <atomic>
#include <future>
#include <sstream>
#include <fstream>


#define N_PKTS 1500000UL
#define PKT_SIZE 9000
#define N_THREADS 8


#define NETMAP_WITH_LIBS
extern "C" {
    #include "net/netmap_user.h"
    #include "libnetmap.h"
    #include "net/netmap.h"
}

/*
* Global arguments for all threads
*/

struct glob_arg {

    int nthreads;
    int cpus; // number of cpus to use
	struct nmport_d *nmd;
	int main_fd;
    int wait_link;
	char ifname[512];
	int forever;
    uint32_t orig_mode; // this is not needed
    int affinity;


};

/* counters to accumulate statistics */
struct stats {
	uint64_t pkts, bytes, events;
	uint64_t drop, drop_bytes;
	uint64_t min_space;
	struct timeval t;
	uint32_t oq_n; /* number of elements in overflow queue (used in lb) */
};

/*
 * Arguments for a new thread. The same structure is used by
 * the source and the sink
 */
struct targ {
	struct glob_arg *g;
	int used;
	int completed;
	int cancel;
	int fd;
	struct nmport_d *nmd;
	/* these ought to be volatile, but they are
	 * only sampled and errors should not accumulate
	 */
	struct stats ctr;

	struct timespec tic, toc;
	int me;
	pthread_t thread;
	int affinity;
    int id;
    void* localBuff;
	//struct pkt pkt;
	void *frame;
	uint16_t seed[3];
	u_int frag_size;
};

static struct targ *targs;


#define MEM_PER_THREAD_BUFF (PKT_SIZE+PKT_SIZE/10*0)*N_PKTS/N_THREADS
/*
* Buffer to hold the captured packets. The vector contains pointers (one per
*  thread), that are mapped to arrays (contigous memory) where the packets are
*  written. The allocation is done later by a thread attached to the NIC 's
*  local NUMA node. In this way we garantee that the memory is allocated on the
*  local node.
*
* The size of each thread buffer is a little bit larger than
* [N_PKTS*PKT_SIZE]/N_THREADS
*/
std::vector<std::shared_ptr<std::array<u_char, MEM_PER_THREAD_BUFF>>> threadBuff(N_THREADS);


//DDDvolatile struct { volatile uint64_t pkts; uint64_t packets[40]; volatile
//DDD    uint64_t bytes; } ctr;



static int setaffinity(pthread_t me, int i)
{


    if (i < 0)
        return 1;

    cpu_set_t cpumask;
    //printf("set affinity to %d\n", i);

    CPU_ZERO(&cpumask);
    CPU_SET(i, &cpumask);

    int s = pthread_setaffinity_np(me, sizeof(cpu_set_t), &cpumask);

    if (s != 0)
    {
        D("Unable to set affinity: %s", strerror(errno));
        return 1;
    }

    return 0;
}

static void * receiver_body(void *data) {



    struct targ *targ = (struct targ *) data;
    if (setaffinity(targ->thread, targ->affinity) == 1){
        printf("Did not set thread affinity!\n");
    }


    struct pollfd pfd = { .fd = targ->fd, .events = POLLIN };
    struct netmap_if *nifp;
    struct netmap_ring *ring;
    struct stats cur;
    //DDDint i;
    memset(&cur, 0, sizeof(cur));



    D("reading from %s fd %d",
        targ->g->ifname, targ->fd);




    nifp = targ->nmd->nifp;
    int ri = targ->nmd->first_rx_ring;
    ring = NETMAP_RXRING(nifp, ri);


    int pollRet = 0;
    int pollPrevState = 0;



    /* Not the actual ThreadID, but just a tag */
    const int thisThreadID = targ->id;

    /* Ptr to the position where next packet has to be written */
    u_char* tbi = &threadBuff[thisThreadID]->at(0);

    while (1){

        pollRet = poll(&pfd, 1, 1000);
        if (pollRet <= 0) {
            if (pollPrevState > 0) {
                //printf("Stopped receiving packets\n");
                //break;
            }
        }
        pollPrevState = pollRet;

        
        if (!nm_ring_empty(ring)) {

            u_int head = ring->head;
            int packetsInRing = nm_ring_space(ring);
            int rx;

            if (packetsInRing > 1000){
                printf("DEBUG: packetsInRing: %d\n", packetsInRing);
            }


            // Loop through packets (non empty slots) in each ring.
            for (rx = 0; rx < packetsInRing; rx++) {
                u_int idx = ring->slot[head].buf_idx;
                u_char *buf = (u_char *)NETMAP_BUF(ring, idx);
                

                /* copy pkt and update pointer */
                tbi = std::copy(buf, buf+PKT_SIZE, tbi);


                if (ring->slot[rx].len != PKT_SIZE)
                {
                    printf("DEBUG: slot->len: %d\n, which is not equal to PKT_SIZE: %d\n", ring->slot[rx].len, PKT_SIZE);
                }


                head = nm_ring_next(ring, head);
            }

            /* prepare to recv next ring */
            ring->head = ring->cur = head;

            /* Update recv counter */
            targ->ctr.pkts += packetsInRing;
        }
    }

    return 0;
}


void *reporting_thread(void *_g) {



	struct glob_arg *g = (struct glob_arg *) _g;
    struct targ *t;

    int prev_pkts = 0;
    while (true) {

        int64_t totalPackets = 0;
	    for (int i = 0; i < g->nthreads; i++) {
	    	t = &targs[i];
	    	printf("Thread %d: %d packets\n", i, t->ctr.pkts);
            totalPackets += t->ctr.pkts;
	    }

        printf("Total packets: %ld\n", totalPackets);

        sleep(2);



        if (totalPackets == prev_pkts && totalPackets > 0) {
            printf("No more packets to receive!\n");
            break;
        }


        prev_pkts = totalPackets;



        //// Check contents of threadBuff
        //for (int k = 0; k < threadBuff.size(); k++)
        //{
        //    int flag = 0;
        //    for (int j = 0; j < MEM_PER_THREAD_BUFF; j+=PKT_SIZE)
        //    {
        //        for (int i = 42; i < PKT_SIZE; i++)
        //        {
        //            if (threadBuff.at(k)->at(j+i) != 50)
        //            {
        //                printf("threadBuff[%d][%d][%d] = %d\n", k, (int)(j+i)/9000, (j+i)%9000,  threadBuff[k]->at(j+i));
        //                flag = 1;
        //                break;
        //            }
        //        }
        //        if (flag == 1)
        //        {
        //            break;
        //        }
        //    }
        //}

        ///* Print packet contents (debug) */
        //for (int k = 0; k < 20 ; k++)
        //{
        //    printf("%d ", threadBuff[2]->at(k));
        //}
        //printf("...\n");
        //for (int k = 0; k < 20 ; k++)
        //{
        //    printf("%d ", threadBuff[1]->at(k));
        //}
        //printf("...\n");


    }

    //* Recvd packets to file */
    for (int i = 0; i < g->nthreads; i++) {

        std::string filename = "recv_" + std::to_string(i) + "_" + std::to_string(time(NULL)) + ".txt";

        // write packets to txt file
        std::ofstream myfile;
        myfile.open(filename);

        struct targ *t = &targs[i];
        int packetsInThreadBuff = t->ctr.pkts;
        for (int j = 0; j < packetsInThreadBuff; j++)
        {
            //for (int k = 0; k < PKT_SIZE; k++)
            for (int k = 0; k < 50; k++)
            {
                myfile << static_cast<unsigned>(threadBuff[i]->at(j*PKT_SIZE + k)) << " ";
            }
            myfile << "\n";
        }

        myfile.close();

    }

    return 0;
}

static int start_threads(struct glob_arg *g) {

    targs = (struct targ *) calloc(g->nthreads, sizeof(*targs));

    struct targ *t;
    for (int i = 0; i < g->nthreads; i++) {
		t = &targs[i];
		bzero(t, sizeof(*t));

		t->fd = -1; /* default, with pcap */
		t->g = g;
        t->id = i; /* Thread ID */



        //int m = -1; // TODO: Check if it's better to enable SW rings. DDD
		//if (g->orig_mode == NR_REG_NIC_SW) {
        //    printf("DDDLOL FUCK YOU\n");
		//	g->nmd->reg.nr_rx_rings;
		//}


        if (i > 0)
        {
            /* the first thread uses the fd opened by the main
			 * thread, the other threads re-open /dev/netmap
			 */

            t->nmd = nmport_clone(g->nmd);
            if (t->nmd == NULL)
				return -1;

            int j = i;
            //printf("t->nmd->reg.nr_mode: %d\n", t->nmd->reg.nr_mode);
			//if (m > 0 && j >= m) {
			//	/* switch to the software rings */
            //    printf("DDDLOL FUCK YOU\n");
			//	t->nmd->reg.nr_mode = NR_REG_ONE_SW;
			//	j -= m;
			//}

            t->nmd->reg.nr_ringid = j & NETMAP_RING_MASK;
            t->nmd->reg.nr_flags |= NETMAP_NO_TX_POLL;


            /* Register interface. */
			if (nmport_open_desc(t->nmd) < 0) {
				nmport_undo_prepare(t->nmd);

                printf("Here be errors\n");

				t->nmd = NULL;
				return -1;
			}
        }
        else
        {
            // Recycle the main thread's nmd
            t->nmd = g->nmd;
        }
        t->fd = t->nmd->fd;


        if (g->affinity >= 0) {
            /* bind the thread to a specific CPU */
            t->affinity = 0 + (g->affinity + i)*2 % g->cpus;
            printf("Thread %d bound to CPU %d\n", i, t->affinity);
        }
        else{
            t->affinity = -1;
        }
    }
    /* Wait for PHY reset. */
	D("Wait %d secs for phy reset", g->wait_link);
	sleep(g->wait_link);
    D("Ready...");


	for (int i = 0; i < g->nthreads; i++) {
		t = &targs[i];
		if (pthread_create(&t->thread, NULL, receiver_body, t) == -1) {
			D("Unable to create thread %d: %s", i, strerror(errno));
			t->used = 0;
		}
	}

    // Create reporting thread
	if (pthread_create(&t->thread, NULL, reporting_thread, g) == -1) {
        D("yolo\n");
    }
    pthread_join(t->thread, NULL);

    return 0;
}

static void * init_memory(void* nothing)
{
    setaffinity(pthread_self(), 1);

    printf("Allocating memory to hold %d packets per thread.\n", MEM_PER_THREAD_BUFF/PKT_SIZE);

    for (int i = 0; i < N_THREADS; i++){
        threadBuff[i] = std::make_shared<std::array<u_char, MEM_PER_THREAD_BUFF>>();
    }

    return 0;
}

int main(int argc, char **argv) {

    struct glob_arg g;
    g.main_fd = -1;


    g.nthreads = N_THREADS;
    g.cpus = 40;
    g.wait_link = 2;
    g.forever = 1;
    g.affinity = 0; //0 to enable, -1 to disable


    // Thread to allocate memory buffers in the local NUMA node
    pthread_t timmy;
    pthread_create(&timmy, NULL, init_memory, NULL);
    pthread_join(timmy, NULL);

    // device name
    strcpy(g.ifname, "netmap:ens7f0");

    g.nmd = nmport_prepare(g.ifname);
    if (g.nmd == NULL) {
        fprintf(stderr, "Unable to open netmap port %s\n", g.ifname);
        return 1;
    }


    // This might be a good upgrade someday
    //int res = nmport_extmem_from_file(g.nmd, "/dev/hugepages/hugefile");


    /* If using many threads, map one thread to one single HW ring */
	g.orig_mode = g.nmd->reg.nr_mode;
	if (g.nthreads > 1) {
		switch (g.orig_mode) {
		case NR_REG_ALL_NIC:
		case NR_REG_NIC_SW: // This is ours
			g.nmd->reg.nr_mode = NR_REG_ONE_NIC;
			break;
		case NR_REG_SW:
			g.nmd->reg.nr_mode = NR_REG_ONE_SW;
			break;
		default:
			break;
		}
		g.nmd->reg.nr_ringid = 0;
	}


    if (nmport_open_desc(g.nmd) < 0){
        fprintf(stderr, "Unable to open netmap port %s\n", g.ifname);
        return 1;
    }


    // get num of queues in rx. TODO: check if this is useful
    int devqueues = g.nmd->reg.nr_rx_rings + g.nmd->reg.nr_host_rx_rings;
    printf("INFO: nr_rx_rings: %d, nr_host_rx_rings: %d, devqueues: %d\n", g.nmd->reg.nr_rx_rings, g.nmd->reg.nr_host_rx_rings, devqueues);


    /* Initialize capturing threads */
	if (start_threads(&g) < 0){
        fprintf(stderr, "Unable to start all threads.\n");
        return 1;
    }


    nmport_close(g.nmd);








    
    printf("DEBUG: Entering Capturer::Debug()! --------------------\n");


    for (int i_thrd = 0; i_thrd < N_THREADS; i_thrd++)
    {
        printf("DEBUG: Thread %d received %lu packets\n", i_thrd, targs[i_thrd].ctr.pkts);
        printf("DEBUG: Writing all of them to a file! (stop this at any moment)\n");

        std::ofstream fout;
        fout.open("thread" + std::to_string(i_thrd) + "_raw.txt");
        fout << "Packet index | Packet[0, ..., 8999]" << std::endl;


        // Where this thread's buffer starts in memory
        uint8_t *data_ptr = threadBuff.at(i_thrd)->data();
        for (int i_pkt = 0; i_pkt < targs[i_thrd].ctr.pkts; i_pkt++)
        {


            // BEGIN PROCESS PACKET
            // Dump it to a file, compare its contents with a reference, etc.

            fout << i_pkt << " | ";

            for (int i_byte = 0; i_byte < PKT_SIZE; i_byte++)
            {
                fout << (int) data_ptr[i_byte] << " ";
            }

            fout << std::endl;


            // END PROCESS PACKET


            // Move ptr to the next packet
            data_ptr += PKT_SIZE;
        }


        fout.close();

    }


    printf("DEBUG: Exiting Capturer::Debug()! --------------------\n");




















    printf("Exiting\n");
}
