
set -e

#sudo ethtool -U ens7f0 rx-flow-hash udp4 v


DEV="ens7f0"
DRV_VER="2.21.12"
DRV_VER="2.20.12"
DRV_VER="2.22.8"
NETMAP_BASE="/home/koto/local/netmap"

# Set Netmap affinity to local NUMA node, etc.
ethtool -L $DEV combined 8
$NETMAP_BASE/LINUX/i40e-$DRV_VER/scripts/set_irq_affinity -x local $DEV
#$NETMAP_BASE/LINUX/i40e-$DRV_VER/scripts/set_irq_affinity -x all $DEV
#$NETMAP_BASE/LINUX/i40e-$DRV_VER/scripts/set_irq_affinity -x 0,2,4,6,8,10,12,14,16,18 $DEV


# promiscuous mode on
ip link set $DEV promisc on
sudo ip link set $DEV mtu 9000


# ethtool defaults
ethtool -C $DEV adaptive-rx on
ethtool -G $DEV rx 512
ethtool -A $DEV tx off rx off
#ethtool -K $DEV tx off rx off gso off tso off gro off lro off


# To use netmap
sudo chmod o+rw /dev/netmap

# Netmap reccomended
ethtool -A $DEV tx off rx off
ethtool -K $DEV tx off rx off gso off tso off gro off lro off

# ethtool modified
ethtool -C $DEV adaptive-rx on
#ethtool -C $DEV rx-usecs 1000
ethtool -G $DEV rx 512
ethtool -X $DEV hkey 00:00:00:00:0A:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00

# Netmap
echo 36864 > /sys/module/netmap/parameters/ring_size
echo 163840 > /sys/module/netmap/parameters/buf_num

echo 9088 > /sys/module/netmap/parameters/buf_size
echo 1 > /sys/module/netmap/parameters/verbose
#echo 83840 > /sys/module/netmap/parameters/buf_num
#echo 147456 > /sys/module/netmap/parameters/ring_size


# sysctl defaults
/sbin/sysctl -w net.core.wmem_max=212992
/sbin/sysctl -w net.core.rmem_max=212992
/sbin/sysctl -w net.ipv4.tcp_rmem="4096	131072	6291456"
/sbin/sysctl -w net.ipv4.tcp_wmem="4096	131072	6291456"
/sbin/sysctl -w net.core.netdev_max_backlog=1000
/sbin/sysctl -w net.ipv4.tcp_timestamps=1
/sbin/sysctl -w net.ipv4.tcp_mtu_probing=0
/sbin/sysctl -w net.ipv4.route.flush=1
/sbin/sysctl -w net.ipv4.tcp_low_latency=0
/sbin/sysctl -w net.ipv4.tcp_sack=1
/sbin/sysctl -w net.core.netdev_max_backlog=250000

# sysctl modified
echo "" > /etc/sysctl.d/99-sysctl-custom.conf
echo "net.core.wmem_max = 104857600" >> /etc/sysctl.d/99-sysctl-custom.conf
echo "net.core.rmem_max = 104857600" >> /etc/sysctl.d/99-sysctl-custom.conf
echo "net.ipv4.tcp_rmem = 4096 524288 104857600" >> /etc/sysctl.d/99-sysctl-custom.conf
echo "net.ipv4.tcp_wmem = 4096 524288 104857600" >> /etc/sysctl.d/99-sysctl-custom.conf
echo "net.core.netdev_max_backlog = 250000" >> /etc/sysctl.d/99-sysctl-custom.conf
echo "net.ipv4.tcp_timestamps = 0" >> /etc/sysctl.d/99-sysctl-custom.conf
echo "net.ipv4.tcp_mtu_probing = 1" >> /etc/sysctl.d/99-sysctl-custom.conf
echo "net.ipv4.tcp_low_latency = 1" >> /etc/sysctl.d/99-sysctl-custom.conf
echo "net.ipv4.tcp_sack = 0" >> /etc/sysctl.d/99-sysctl-custom.conf
echo "net.core.netdev_max_backlog=30000" >> /etc/sysctl.d/99-sysctl-custom.conf
echo "net.ipv4.route.flush = 1" >> /etc/sysctl.d/99-sysctl-custom.conf

# reload 
#sysctl -p /etc/sysctl.d/99-sysctl-custom.conf



